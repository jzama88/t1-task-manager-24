package com.t1.alieva.tm.exception.user;

import javax.naming.NamingSecurityException;

public final class AuthenticException extends NamingSecurityException {

    public AuthenticException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }
}
