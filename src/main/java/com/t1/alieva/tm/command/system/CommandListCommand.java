package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "commands";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application commands.";
    }

    @Override
    public void execute() {
        System.out.println("COMMANDS ");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }
}
