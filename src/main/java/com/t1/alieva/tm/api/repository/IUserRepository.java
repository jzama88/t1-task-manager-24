package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String email);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);
}
