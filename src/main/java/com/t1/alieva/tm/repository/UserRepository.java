package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public User create(@NotNull String login, @NotNull String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    @NotNull
    public User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull String email) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role) {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    @Nullable
    public User findOneByLogin(@NotNull String login) {
        return models
                .stream()
                .filter(r -> login.equals(r.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findOneByEmail(@NotNull String email) {
        return models
                .stream()
                .filter(r -> email.equals(r.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(r -> login.equals(r.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(r -> email.equals(r.getEmail()));
    }
}
